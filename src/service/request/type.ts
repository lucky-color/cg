import type { AxiosRequestConfig, AxiosResponse } from "axios";
export interface XMRequestInterceptors<T = AxiosResponse> {
    requestInterceptor?: (config: any) => any
    requestInterceptorCatch?: (error: any) => any
    responseInterceptor?: (res: T) => T
    responseInterceptorCatch?: (error: any) => any
}

export interface XMRequestConfig<T = AxiosResponse> extends AxiosRequestConfig { 	
    interceptors?: XMRequestInterceptors<T>
}
