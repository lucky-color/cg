import axios from 'axios'
import type { AxiosInstance } from 'axios'
import type { XMRequestInterceptors, XMRequestConfig } from './type'

class XMRequest {
    instance: AxiosInstance
    interceptors?: XMRequestInterceptors
    constructor(config: XMRequestConfig) {
        // 创建axios实例
        this.instance = axios.create(config)

        // 保存基本信息
        this.interceptors = config.interceptors // 可以保存请求拦截器和响应拦截器等信息到这里面去。

        // 使用拦截器
        //从config钟取出的拦截器是对应的实例的拦截器
        this.instance.interceptors.request.use(
            this.interceptors?.requestInterceptor,
            this.interceptors?.requestInterceptorCatch
        )

        this.instance.interceptors.response.use(
            this.interceptors?.responseInterceptor,
            this.interceptors?.responseInterceptorCatch
        )


        // 所有的实例都有的拦截器
        this.instance.interceptors.request.use(
            config => {
                return config
            },
            err => {
                return err
            }
        ) 		

        this.instance.interceptors.response.use(
            res => {
                return res.data
            },
            err => {
                if(err.response.status === 404) {
                    console.log('404错误');
                }
                return err
            }
        )
    }

    request<T>(config: XMRequestConfig<T>): Promise<T> {

        return new Promise((resolve, reject) => {

    //单个请求对请求config的处理

            if (config.interceptors?.requestInterceptor) {

                config = config.interceptors.requestInterceptor(config)

            }


     this.instance.request<any, T>(config).then((res) => {

     //单个请求对数据的处理

        if (config.interceptors?.responseInterceptor) {

            res = config.interceptors.responseInterceptor(res)

        }

            console.log(res)
        //将结果返回出去
           resolve(res)

    }).catch((err) => {

            reject(err)

            return err

    })

     })

    }


    get<T>(config: XMRequestConfig<T>): Promise<T> {

        return this.request<T>({ ...config, method: 'GET' })

    }


    post<T>(config: XMRequestConfig<T>): Promise<T> {

         return this.request<T>({ ...config, method: 'POST' })

    }


    delete<T>(config: XMRequestConfig<T>): Promise<T> {

         return this.request<T>({ ...config, method: 'DELETE' })

    }


    patch<T>(config: XMRequestConfig<T>): Promise<T> {

        return this.request<T>({ ...config, method: 'PATCH' })

    }
}

export default XMRequest