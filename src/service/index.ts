import XMRequest from "./request";
import { BASE_URL, TIME_OUT } from "./request/config";
const xmRequest = new XMRequest({
    baseURL: BASE_URL,
    timeout: TIME_OUT,
    interceptors: {
        requestInterceptor: (config) => {
            const token = ''
            if(token) {
                config.headers!.Authorization = token
            }
            console.log('请求成功拦截');
            return config
        },
        requestInterceptorCatch(error) {
            console.log('响应成功拦截');
            return error
        },
        responseInterceptor: (config) => {
            console.log('响应成功拦截');
            return config
        },
        responseInterceptorCatch(error) {
            console.log('响应失败拦截');
            return error
        },
    }
})

export default xmRequest