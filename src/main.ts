import { createApp } from 'vue'
import App from './App.vue'
import { router} from './router'
import naive from 'naive-ui'

// 引入初始化css
import '@/assets/css/index.css'
import 'normalize.css'

import 'unocss'
createApp(App).use(router).use(naive).mount('#app')