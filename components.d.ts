/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    Login: typeof import('./src/components/basc/login.vue')['default']
    NButton: typeof import('naive-ui')['NButton']
    NCard: typeof import('naive-ui')['NCard']
    NCheckbox: typeof import('naive-ui')['NCheckbox']
    NCol: typeof import('naive-ui')['NCol']
    NForm: typeof import('naive-ui')['NForm']
    NFormItem: typeof import('naive-ui')['NFormItem']
    NInput: typeof import('naive-ui')['NInput']
    NMessageProvider: typeof import('naive-ui')['NMessageProvider']
    NRow: typeof import('naive-ui')['NRow']
    NSpace: typeof import('naive-ui')['NSpace']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
  }
}
