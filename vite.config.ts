import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import * as path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  // 配置模块解析的选项，例如别名
  resolve: {
    // 定义模块的别名 例如 '@'代表'src'目录  vite打包的时候就把 @ 解析为 src
    alias: {
      '@': path.resolve(__dirname, 'src')
    },
  },
  // 配置vite的插件 例如 vue() 是一个用来支持Vue单文件组件的插件
  plugins: [
    vue(),
    AutoImport({
      imports: [
        'vue',
        {
          'naive-ui': [
            'useDialog',
            'useMessage',
            'useNotification',
            'useLoadingBar'
          ]
        }
      ]
    }),
    Components({
      resolvers: [NaiveUiResolver()]
    })
  ],
  // 配置开发服务器的选项 例如端口号，热更新等
  // server: {
  //   port: 8080, // 指定开发服务器启动端口号
  //   // 配置热更新的选项 
  //   // hmr: {
  //   //   host: '127.0.0.1',
  //   //   port: 8080
  //   // },
  //   proxy: {
  //     '/api': {
  //       // 代理到指定的目标地址
  //       target: 'http://127.0.0.1:8090',
  //       // 修改请求头中的host为目标地址的host
  //       changeOrigin: true,
  //       // 重写路径
  //       rewrite: (path: string) => path.replace(/\^api/, '')
  //     }
  //   }
  // },
  build: {
    // 生成原映射文件，调试定位到源代码的错误位置
    sourcemap: false,
    // 压缩生成的代码 -> 减少代码的体积和加载时间 在这里使用terser插件来压缩代码
    minify: 'terser',
    // terser插件的配置参数 -> 自定义压缩的行为和效果
    terserOptions: {
      compress: {
        // 生产环境时移除console.log()
        drop_console: true,
        // 生产环境移除debugger语句
        drop_debugger: true,
      },
    },
  },
})
