## 环境配置

- [pnpm](https://pnpm.js.org/)
- IDE [VSCode](https://code.visualstudio.com/)
- 代码格式化工具 [Eslint](https://eslint.org/)
  - 代码风格 [@antfu/eslint-config](https://github.com/antfu/eslint-config)
- 插件
  - [Eslint](https://github.com/Microsoft/vscode-eslint)
  - [Vue Language Features (Volar)](https://github.com/johnsoncodehk/volar)

## 技术栈

- [Vue3](https://v3.vuejs.org/)
- [Vite](https://vitejs.dev/)
- [TypeScript](https://www.typescriptlang.org/)
- [Navie](https://www.naiveui.com/zh-CN/)  Vue 3 组件库

## 安装使用

- 安装依赖 `pnpm install`
- 运行 `pnpm run dev`
- 打包 `pnpm run build`

## 目前页面

- 登录页 `/login`

